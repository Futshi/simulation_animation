/* global PIXI */

import { TEXTURES, WINDOW, VORONOI } from './constants.js';
import { Voronoi } from './voronoi.js';
import { Sprite } from './sprite.js';

export class VoronoiObject extends Sprite {
  constructor(sprite) {
    super(sprite);
    this.speed = VORONOI.speed;
  }
  
  getFragments() {
    let fragments = [];
    // generate (random) voronoi points
    let voronoi_cells = [];
    for(let i = 0; i < VORONOI.fracture_points; i++) {
      let x = Math.floor(Math.random() * this.sprite.width) - 1;
      let y = Math.floor(Math.random() * this.sprite.height) - 1;
      voronoi_cells.push([{x, y}]); // push array into array (aka double array, duh)
    }
    // compute voronoi cells
    for(let i = 0; i < this.sprite.width; i++) {
      for(let j = 0; j < this.sprite.height; j++) {
        let best_distance = 0;
        let best_index = -1;
        // find closest point (located at first index in second dimension @ voronoi_points)
        for(let k = 0; k < VORONOI.fracture_points; k++) {
          let distance = Math.sqrt(
            Math.pow(i - voronoi_cells[k][0].x, 2)
            + Math.pow(j - voronoi_cells[k][0].y, 2)
          );
          // set first value if none available (since infinity and other stuff seems not writeable in JS)
          if(distance < best_distance || best_index == -1) {
            best_distance = distance;
            best_index = k;
          }
        }
        // set pixel to according cell
        voronoi_cells[best_index].push({x: i, y: j});
      }
    }
    // create voronoi fragments
    for(let i = 0; i < VORONOI.fracture_points; i++) {
      const voronoi = new Voronoi(PIXI.Sprite.from(PIXI.Loader.shared.resources[TEXTURES.voronoi].texture));
      voronoi.setPosition(this.sprite.x, this.sprite.y);
      voronoi.generateMask(voronoi_cells[i]);
      fragments.push(voronoi);
    }
    return fragments;
  }
  
  moveUntilOutOfArea() {
    // move unfragmented object
    this.sprite.y += this.speed;
    if(this.sprite.y - this.sprite.height > WINDOW.height) {
      return true;
    }
    return false;
  }
}