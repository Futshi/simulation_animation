export const TEXTURES = {
  level: './assets/desert-backgorund.png',
  player: './assets/ship.png',
  bullet: './assets/laser-bolts.png',
  particle: './assets/explosion.png',
  enemy: './assets/enemy-small.png',
  health: './assets/power-up.png',
  planet_large: './assets/planets/Desert.png',
  planet_medium: './assets/planets/Terran.png',
  planet_small: './assets/planets/Baren.png',
  planet_tiny: './assets/planets/Ice.png',
  voronoi: './assets/planets/Lava.png'
};

export const WINDOW = {
  x: 0,  // caution: not used in app
  y: 0,  // caution: not used in app
  width: 400,
  height: 600
}

export const CONTROLS = {
  move_left: ["KeyA", "ArrowLeft"],
  move_right: ["KeyD", "ArrowRight"],
  move_up: ["KeyW", "ArrowUp"],
  move_down: ["KeyS", "ArrowDown"],
  shoot: ["Space"],
  pause: ["KeyP"],
};

export const DEVELOPMENT = {
  god_mode: false,
  enemies_active: true,
  planets_active: true,
  voronoi_active: true,
}

export const CATMULLROM = {
  num_samples: 600,
  samples_per_segment: 100,
};

export const GAME = {
  cooldown: 10.0
};

export const PLAYER = {
  speed: 2.75,
  health: 5.0,
  shoot_cooldown: 25
};

export const HEALTH = {
  padding: 12.5,
  radius: 2.5
}

export const BULLET = {
  speed: 12.5,
};

export const EXPLOSION = {
  particlesPerExplosion: 10,
};

export const ENEMY = {
  speed: 0.5, // maybe randomize? (currently not used)
  collision_damage: 1.0
};

export const ODES = {
  rk4: 'rk4',
  verlet: 'verlet',
}

export const PHYSICS = {
  ode: ODES.verlet,
}

export const PLANET = {
  health: 1.0,
  collision_damage: 1.0,
  radius: {
    medium: 60,
    small: 25,
    tiny: 10
  },
  scale: {
    large: 1.5,
    medium: 0.75,
    small: 0.33,
    tiny: 0.125
  },
  speed: {
    large: 0.75,
    medium: 0.005,
    small: 0.0075,
    tiny: 0.01
  }
};

export const VORONOI = {
  collision_damage: 1.0,
  speed: 1.75,
  fracture_damping: 0.75,
  fracture_points: 5
}

export const SPAWNER = {
  enemy_cooldown: 50.0,
  planet_cooldown: 1250.0,
  voronoi_cooldown: 125.0
};
