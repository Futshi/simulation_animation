/* global PIXI */

import { WINDOW, VORONOI } from './constants.js';
import { Sprite } from './sprite.js'

export class Voronoi extends Sprite {
  constructor(sprite) {
    super(sprite); // parent constructor
    this.speed = VORONOI.speed * VORONOI.fracture_damping;
    this.fracture_speed = {
      x_sign: Math.random() < 0.5 ? -1 : 1,
      y_sign: Math.random() < 0.5 ? -1 : 1,
      x: Math.min(0.05, Math.random() / 5),
      y: Math.min(0.05, Math.random() / 5)
    };
    this.mask;
  }
  
  getMask() { return this.mask; }
  
  setMask(mask) { this.sprite.mask = mask; }
  
  generateMask(pixels) {
      const mask = new PIXI.Graphics();
      mask.beginFill(0x00ff00);
      mask.lineStyle(0);
      for(let i= 0; i < pixels.length; i++) {
        mask.drawRect(pixels[i].x, pixels[i].y, 1, 1);
      }
      mask.endFill();
      this.mask = mask;
      this.sprite.mask = this.mask;
  }
  
  moveUntilOutOfArea() { // true = out of area
    this.sprite.x += this.fracture_speed.x_sign * this.fracture_speed.x;
    this.sprite.y += this.speed + this.fracture_speed.y_sign * this.fracture_speed.y;
    // move mask
    this.mask.x = this.sprite.x;
    this.mask.y = this.sprite.y;
    if(this.sprite.y - this.sprite.height > WINDOW.height) {
      return true;
    }
    return false;
  }
}
