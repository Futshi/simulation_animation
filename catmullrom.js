/* global vec2 */

import { CATMULLROM } from './constants.js';

// Holds a lookup table generated from control positions.
// Provides helper methods for easy evaluation of the lut.
export class Spline {
  // control points should be [{x: 0, y: 0},]
  constructor(control_points) {
    this.controls = control_points;
    this.lookup = this.generateChordLengthLookup();
  }

  // this helper method returns X and Y coordinates for spline position defined
  // by 4 control points and interpolation t
  static cmr2d(t, v0, v1, v2, v3) {
    return [
      Spline.cmr(t, v0[0], v1[0], v2[0], v3[0]),
      Spline.cmr(t, v0[1], v1[1], v2[1], v3[1])
    ];
  }

  // evaluates spline using 4 control points v0 to v3
  // with t from 0..1
  static cmr(t, p0, p1, p2, p3) {
    return 0.5 * (
      (2 * p1) +
      (-p0 + p2) * t +
      (2 * p0 - 5 * p1 + 4 * p2 - p3) * t * t +
      (-p0 + 3 * p1 - 3 * p2 + p3) * t * t * t
    );
  }

  binSearchLut(x, start, end){
    if(start > end) return end;

    let mid = Math.floor((end + start)/2);

    if(this.lookup[mid] === x) return mid;

    if(this.lookup[mid] > x)
      // recurse left half
      return this.binSearchLut(x, start, mid-1);
    else
      // recurse right half
      return this.binSearchLut(x, mid+1, end);
  }

  searchLut(t) {
    let index = this.binSearchLut(t, 0, this.lookup.length);
    return index/CATMULLROM.num_samples;
  }

  // sample control points for t normalized to (0 .. 1)
  normalizedSample(t) {
    let num_segments = this.controls.length - 2;
    // scale t to span from 0 to num_segments
    let t_scaled = t * (num_segments - 1);
    let control_index = Math.floor(t_scaled);
    // interpolation t within segment (0 .. 1)
    let t_segment = t_scaled - control_index;

    return Spline.cmr2d(t_segment, this.controls[control_index], this.controls[control_index+1], this.controls[control_index+2], this.controls[control_index+3]);
  }

  constantSpeedSample(t) {
    let t_const = t - Math.floor(t);
    return this.normalizedSample(this.searchLut(t_const));
  }

  generateChordLengthLookup(){
  // Sample the curve at a multitude of parametric values.
    let parametric_points = new Array(CATMULLROM.num_samples).fill(0).map(function(value, index){
      return this.normalizedSample(index/CATMULLROM.num_samples);
    }, this);

    // Compute the linear distance between adjacent points.
    let chord_lengths = parametric_points.map(function(value, index){
      if(index == parametric_points.length-1){
        return null;
      }
      let result = [];
      vec2.sub(result, parametric_points[index + 1], parametric_points[index])
      return vec2.length(result);
    });

    // There are #parametric_points - 1 chord lengths
    chord_lengths.splice(chord_lengths.length-1, 1);

    // Normalize chord lengths to length 1
    let sum = chord_lengths.reduce((a,b) => a + b, 0);
    let chord_length_norm = chord_lengths.map(x => x/sum)

    // make sure lut has a 0 element
    let lut = new Array(CATMULLROM.num_samples).fill(0);
    // accumulate chord lengths in lut
    for(let index = 1; index < lut.length; index++){
      lut[index] = lut[index-1] + chord_length_norm[index-1]
    }
    return lut;
  }
}
