/* global PIXI */
import {TEXTURES, EXPLOSION} from './constants.js'
import { Flare } from './flare.js';

export class ExplosionSpawner {
  constructor(particleSystem, game_container) {
    this.texture = PIXI.Loader.shared.resources[TEXTURES.particle].texture;
    this.game_container = game_container;
    this.particleSystem = particleSystem;
    this.particleCount = EXPLOSION.particlesPerExplosion;
    this.flares = [];
  }

  spawnExplosion(position) {
    for(let i = 0; i < this.particleCount; i++) {
      let sprite = new PIXI.Sprite(this.texture);
      let flare = new Flare(sprite, this.particleSystem.addParticle());
      flare.setSpawnPosition(position);

      let v = vec3.create();
      vec3.random(v, 10);
      flare.particle.setVelocity(v);

      this.flares.push(flare);
      this.game_container.addChild(flare.getSprite());
    }
  }

  update(dt) {
    for(let i = 0; i < this.flares.length; i++) {
      let flare = this.flares[i];
      if(flare.moveUntilEndOfLife(dt)) {
          this.game_container.removeChild(flare.getSprite());
          this.flares.splice(i, 1);
          this.particleSystem.removeParticle(flare.particle);
      }
    }
  }
}
