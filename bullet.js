import { BULLET, WINDOW } from './constants.js';
import { Sprite } from './sprite.js';

export class Bullet extends Sprite {
  constructor(sprite) {
    super(sprite); // parent constructor
  }
  
  setSpawnPosition(position) {
    this.sprite.x = position.x
    this.sprite.y = position.y;
  }
  
  moveUntilOutOfArea() { // move bullet
    this.sprite.y -= BULLET.speed;
    if(this.sprite.y < WINDOW.y) {
      return true;
    }
    return false;
  }
}