export class Sprite {
  constructor(sprite) {
    this.sprite = sprite;
  }
  
  getSprite() {
    return this.sprite;
  }
  
  getSize() {
    return {
      width: this.sprite.width,
      height: this.sprite.height
    };
  }
  
  getPosition() {
    return {
      x: this.sprite.x,
      y: this.sprite.y
    };
  }
  
  setPosition(x, y) { 
    this.sprite.x = x;
    this.sprite.y = y;
  }
  
  setScale(scale) {
    this.sprite.anchor.set(0.5);
    this.sprite.scale.set(scale, scale);
  }
}