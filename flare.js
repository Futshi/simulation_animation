import { Sprite } from './sprite.js';

const MASS = {min:10, max:20};
const LIFETIME = {min:10, max:100};

export class Flare extends Sprite {
  constructor(sprite, particle) {
    super(sprite); // parent constructor
    this.particle = particle;

    // give the particle a random mass between min and max values
    this.particle.setMass((Math.random() * (MASS.max - MASS.min + 1)) + MASS.min);
    this.lifetime = (Math.random() * (LIFETIME.max - LIFETIME.min + 1)) + LIFETIME.min;
    this.time_alive = 0.0;
  }

  setSpawnPosition(position) {
    this.sprite.x = position.x;
    this.sprite.y = position.y;
    this.particle.setPosition([position.x, position.y, 0]);
  }

  moveUntilEndOfLife(dt) {
    this.time_alive += dt;
    if(this.time_alive > this.lifetime){
      return true;
    }

    let pos = this.particle.getPosition();
    this.sprite.x = pos[0];
    this.sprite.y = pos[1];
    return false;
  }
}
