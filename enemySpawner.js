/* global PIXI */

import { TEXTURES, SPAWNER, WINDOW } from './constants.js'
import { Enemy } from './enemy.js'

export class EnemySpawner {
  constructor(enemy_sprite) {
    this.enemy_sprite = enemy_sprite;
    this.cooldown = SPAWNER.enemy_cooldown;
  }

  getRandomXSpawnPosition() {
    let min_x = WINDOW.x;
    let max_x = WINDOW.width - this.enemy_sprite.width;
    return Math.floor(Math.random() * (max_x - min_x + 1)) + min_x;
  }

  getNewEnemy() {
    const enemy = new Enemy(PIXI.Sprite.from(PIXI.Loader.shared.resources[TEXTURES.enemy].texture));
    enemy.setSpawnPosition(this.getRandomXSpawnPosition(), -this.enemy_sprite.height);
    return enemy;
  }

  removeEnemy(i) {
    this.game.removeChild(this.enemies[i]);
    this.enemies.splice(i, 1);
  }

  spawnNewEnemyCheck(delta) {
    // enemy spawn interval
    this.cooldown += delta;
    if(this.cooldown >= SPAWNER.enemy_cooldown) {
      this.cooldown = 0.0;
      return true;
    }
    return false;
  }
}