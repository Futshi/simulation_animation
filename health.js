import { HEALTH } from './constants.js';
import { Sprite } from './sprite.js';

export class Health extends Sprite {
  constructor(sprite) {
    super(sprite); // parent constructor
    this.theta = Math.random() * 360;
    this.clockwise = Math.round(Math.random());
    this.speed = Math.min(0.015, Math.random() / 20);
    this.radius = HEALTH.radius;
  }
  
  setRadius(radius) {
    this.radius = radius;
  }
  
  move(player_position) {
    this.theta = (this.theta + this.speed) % 360;
    this.sprite.x = player_position.x + this.radius;
    this.sprite.y = player_position.y + this.radius;
    
    let sin = Math.sin(this.theta);
    let cos = Math.cos(this.theta);
    // translate point back to origin
    let origin_x = this.sprite.x - player_position.x;
    let origin_y = this.sprite.y - player_position.y;
    // rotate point
    let new_x = this.clockwise
      ? origin_x * cos + origin_y * sin    // clockwise
      : origin_x * cos - origin_y * sin;   // counter-clockwise
    let new_y = this.clockwise
      ? -origin_x * sin + origin_y * cos   // clockwise
      : origin_x * sin + origin_y * cos;   // counter-clockwise
    // translate point back
    this.sprite.x = new_x + player_position.x;
    this.sprite.y = new_y + player_position.y;
  }
}