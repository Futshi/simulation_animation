import { WINDOW, PLANET } from './constants.js';

export class PlanetSystem {
  constructor() {
    this.position = {
      x: 0, y: 0
    };
    this.planet_1;
    this.planet_2;
    this.planet_3;
    this.planet_4;
  }

  getPlanets() {
    return [this.planet_1, this.planet_2, this.planet_3, this.planet_4];
  }

  setSpawnPosition(x, y) {
    this.position.x = x;
    this.position.y = y;
  }

  setPlanets(planet_1, planet_2, planet_3, planet_4) {
    this.planet_1 = planet_1;
    this.planet_1.setPosition(this.position.x, this.position.y);
    this.planet_1.setScale(PLANET.scale.large);
    this.planet_1.setSpeed(PLANET.speed.large);
    this.planet_2 = planet_2;
    this.planet_2.setScale(PLANET.scale.medium);
    this.planet_2.setSpeed(PLANET.speed.medium);
    this.planet_2.setRadius(PLANET.radius.medium);
    this.planet_3 = planet_3;
    this.planet_3.setScale(PLANET.scale.small);
    this.planet_3.setSpeed(PLANET.speed.small);
    this.planet_3.setRadius(PLANET.radius.small);
    this.planet_4 = planet_4;
    this.planet_4.setScale(PLANET.scale.tiny);
    this.planet_4.setSpeed(PLANET.speed.tiny);
    this.planet_4.setRadius(PLANET.radius.tiny);
  }

  moveUntilOutOfArea() {
    let out_of_area = true;
    if(!this.planet_1.moveUntilOutOfArea()) { out_of_area = false; }
    if(!this.planet_2.moveAroundParent(this.planet_1.getPosition(), this.planet_1.getSize().height)) { out_of_area = false; }
    if(!this.planet_3.moveAroundParent(this.planet_2.getPosition(), this.planet_2.getSize().height)) { out_of_area = false; }
    if(!this.planet_4.moveAroundParent(this.planet_3.getPosition(), this.planet_3.getSize().height)) { out_of_area = false; }
    return out_of_area;
  }
}
